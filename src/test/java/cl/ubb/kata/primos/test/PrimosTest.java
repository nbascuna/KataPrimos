package cl.ubb.kata.primos.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cl.ubb.kata.primos.PrimoException;
import cl.ubb.kata.primos.Primos;

public class PrimosTest {
	private Primos primos;
	@Before
	public void SetUp(){
		primos=new Primos();
	}
	
	@Test (expected=PrimoException.class)
	public void IngresarUnoyRetorneError() throws PrimoException{
		//arrange
		//act
		primos.numerosPrimos(1);
		//assert
	}
	
	@Test 
	public void IngresarMenosCuatroyRetorneError() throws PrimoException{
		//arrange
		//act
		primos.numerosPrimos(-4);
		//assert
	}
	
	@Test
	public void IngresarSieteyRetorneSiete() throws PrimoException{
		//arrange
		//act
		//assert
		assertEquals(7, primos.numerosPrimos(7));
	}
	
	@Test (expected=PrimoException.class)
	public void IngresarSeisyRetorneSeis() throws PrimoException{
		//arrange
		//act
		//assert
		assertEquals(6, primos.numerosPrimos(6));
	}
	
	@Test
	public void IngresarDiecinueveyRetorneDiecinueve() throws PrimoException{
		//arrange
		//act
		//assert
		assertEquals(19, primos.numerosPrimos(19));
	}

}
